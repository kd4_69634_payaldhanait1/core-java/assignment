package Date;

public class Date {
	int month;
	int day;
	int year;

	public Date(int m, int d, int y) {
		month = m;
		day = d;
		year = y;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public void setYear(int Years) {
		year = Years;
	}

	public void setMonth(int Months) {
		if (Months >= 0 && Months <= 12)
			month = Months;
	}

	public void setDay(int Days) {
		if (Days >= 0 && Days <= 30)
			day = Days;
	}

	void displayDate() {
		System.out.println(getMonth() + "/" + getDay() + "/" + getYear());
	}

}
