package Shape;

import java.util.Scanner;

public class Circle extends Shape {
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}

	private int radius;

	public Circle() {
		this.radius = 0;
	}

	@Override
	public void claculateArea() {
		this.area = 3.14 * this.radius * this.radius;
		System.out.println(toString());
	}

	@Override
	public void accceptRecord() {
		System.out.println("Enter Radius = ");
		this.radius = new Scanner(System.in).nextInt();
	}

}
