/*
 * . Accept a integer number and when the program is executed print the 
binary, octal and hexadecimal equivalent of the given number
 */



import java.util.Scanner;

public class ToBin {

	public static void main(String[] args) {
		System.out.println("Enter a Number:");
		Scanner sc = new Scanner (System.in);
		int a = sc.nextInt();
		System.out.println("****************************************");
		System.out.println("\tGiven NO.:"+a);
		System.out.println("\tBinary equivalent:"+  Integer.toBinaryString(a) );
		System.out.println("\tOctal equivalent:"+ Integer. toOctalString(a) );
		System.out.println("\tHexadecimal equivalent:"+ Integer. toHexString(a) );
		System.out.println("****************************************");
	}

}
