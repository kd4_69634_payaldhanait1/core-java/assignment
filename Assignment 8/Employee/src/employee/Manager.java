package employee;

public class Manager implements Emp {
	double basicSalary;
	double dearanceAllowance;

	public Manager(double basicSalary, double dearanceAllowance) {
		super();
		this.basicSalary = basicSalary;
		this.dearanceAllowance = dearanceAllowance;
	}

	

	@Override
	public double getSal() {

		return (basicSalary + dearanceAllowance);
	}

	@Override
	public double calcIncentives() {
		
		return this.basicSalary*0.2;
	}

}
