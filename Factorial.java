import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		System.out.println("Enter a number: ");
		Scanner sc = new Scanner (System.in);
		int a = sc.nextInt();
		int sum=1;
		for(int i=a;i>0;i--)
		{
			sum = sum*i;
		}
		System.out.println("Factorial:"+ sum);

	}

}
