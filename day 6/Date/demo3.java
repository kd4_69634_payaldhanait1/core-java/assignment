import java.util.Scanner;

class Date {
	int day;
	int month;
	int year;

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}

	void acceptDate(Date date) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Day:");
		setDay(sc.nextInt());
		System.out.println("Enter Month:");
		setMonth(sc.nextInt());
		System.out.println("Enter Year:");
		setYear(sc.nextInt());
	}

	void displayDate(Date date) {
		System.out.println(this.day + "/" + this.month + "/" + this.year);
	}

	int dateTest(Date date,int flag)
	{
		if (date.day > 0 && date.day <= 31) 
		{
			if (date.month > 0 && date.month <= 12) 
			{
				if (date.year > 1960 && date.year <= 2022)
				{
					flag=0 ;
				}
			}
		} else 
		{
			flag=1;
		}
		return flag;

	}
}

public class demo3 {

	public static void main(String[] args) {

		System.out.println("Enter date:");
		Date date = new Date(0, 0, 0);
		date.acceptDate(date);
		int flag=0;
		int test=date.dateTest(date,flag);
		if (test==0) {
			date.displayDate(date);
		} else {
			System.out.println("Invalid Date!!");
		}

	}

}
