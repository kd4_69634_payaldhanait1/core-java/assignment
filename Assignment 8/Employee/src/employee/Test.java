package employee;

public class Test {

	public static void main(String[] args) {
		Emp e[] = new Emp[3];
		e[0] = new Manager(20000, 500);
		e[1] = new Labour(350, 3000);
		e[2] = new Clerk(2000);
		
		System.out.println("\n*** MANAGER ***");
		System.out.println("Total:"+ Emp.calcTotalIncome(e));
		System.out.println("Manager Salary="+ e[0].getSal());
		System.out.println("Manager Incentive="+ e[0].calcIncentives());
		
		System.out.println("\n*** LABOUR ***");
		System.out.println("Total:"+ Emp.calcTotalIncome(e));
		System.out.println("LABOUR Salary="+ e[1].getSal());
		System.out.println("LABOUR Incentive="+ e[1].calcIncentives());
		
		System.out.println("\n*** CLERK ***");
		System.out.println("Total:"+ Emp.calcTotalIncome(e));
		System.out.println("CLERK Salary="+ e[2].getSal());
	}

}
