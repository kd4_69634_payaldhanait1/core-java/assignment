package demo1;
class Parent
{
	private int num1 = 0;
	private int num2 = 0;
	private int num3 = 0;
	void f1() {
		System.out.println("Parent f1()");
	}
	void f2() {
		System.out.println("Parent f2()");
	}
}

class Child extends Parent
{
	private int num4 = 0;
	void f3() {
		System.out.println("Child f3()");
	}
	
}
public class Test {

	public static void main(String[] args) {
		Child c = new Child();
		c.f1();
		c.f2();
		c.f3();

		Parent p = new Parent();
		p.f1();
		p.f2();
//	 p.f3();//We cannot call child class methods using parent class
//		 instance/reference
Parent p1 = new Child();// upcasting
		p.f1();
		p.f2();
//		// p.f3(); NOT OK -> Object slicing
		Child c = (Child) p;// downcasting
		c.f3();

		// Parent p1 = new Parent();
//		// Child c1 = (Child) p1;
//		Child c1 = (Child) new Parent();
//		c1.f1();
//		c1.f2();
//		c1.f3();
	}

		
	}


