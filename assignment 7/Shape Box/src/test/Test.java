package test;

import java.util.Scanner;

import Shape.Circle;
import Shape.Rectangle;
import Shape.Shape;
import Shape.Square;

class Box<T extends Shape> {
	T obj;

	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}

	void calc() {
		obj.claculateArea();
	}

	void display() {
		System.out.println(obj.toString());
	}

}

public class Test {

	public static void main(String[] args) {

		Box<Rectangle> obj1 = new Box<>();
		obj1.setObj(new Rectangle());
		obj1.getObj().accceptRecord();
		obj1.getObj().claculateArea();

		Box<Square> obj2 = new Box<>();
		obj2.setObj(new Square());
		obj2.getObj().accceptRecord();
		obj2.getObj().claculateArea();

		Box<Circle> obj3 = new Box<>();
		obj3.setObj(new Circle());
		obj3.getObj().accceptRecord();
		obj3.getObj().claculateArea();

	}

}