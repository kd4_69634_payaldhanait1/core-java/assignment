package demo2;

public class Parent {
	private int num1;
	protected int num2;
	public int num3;
	int num4;

	public Parent() {
		System.out.println("Inside Parent::Parameterless");
		this.num1 = 0;
		this.num2 = 0;
		this.num3 = 0;
		this.num4 = 0;
	}

	public void f1() {
		System.out.println("Inside Parent:f1()");
	}

	public void f2() {
		System.out.println("Inside Parent:f2()");
	}

	public void printParent() {
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
	}
}

class Child extends Parent {
	public Child() {
		System.out.println("Inside Child::Parameterless");
	}

	public void f3() {
		System.out.println("Inside Child:f3()");
	}

	void printChild() {
		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
	}
}

class Demo {
	void printDemo() {
		Parent p = new Parent();
		//System.out.println(p.num1); // NOT OK -> Private
		System.out.println(p.num2);// OK -> Protected
		System.out.println(p.num3); // OK -> Public
		System.out.println(p.num4);// OK -> Package Level Private
	}
}
//
//
//
//
//
//
//
//
