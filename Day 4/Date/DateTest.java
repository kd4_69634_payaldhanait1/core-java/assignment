package Date;

import java.util.Scanner;

import Date.*;

public class DateTest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Date D = new Date(0, 0, 0);
		System.out.println("******Enter date******");
		System.out.print("Enter Year:");
		D.setYear(sc.nextInt());
		System.out.print("Enter Month:");
		D.setMonth(sc.nextInt());
		System.out.print("Enter Day:");
		D.setDay(sc.nextInt());
		D.displayDate();

	}

}
