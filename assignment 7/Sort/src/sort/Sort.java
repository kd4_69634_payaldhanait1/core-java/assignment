package sort;

interface Comparator<T> {
	int compare(T obj1, T obj2);
}
//class for comparison 
class customComparator implements Comparator<Integer> {

	@Override
	public int compare(Integer obj1, Integer obj2) {
		int diff = obj1 - obj2;
		return -diff;
	}

}

public class Sort {
	public static <T> void printArray(T[] arr) {
		for (T ele : arr)
			System.out.println(ele);
		System.out.println("Number of elements printed: " + arr.length);
	}

	public static <T> void selectionSort(T[] arr, Comparator c) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				if (c.compare(arr[i], arr[j]) > 0) {
					T temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

	public static void main(String[] args) {

		Integer[] arr = { 100, 20, 130,50,23,11 };
		customComparator c = new customComparator() ;
		//printArray(arr); // printArray<Integer> -- Integer type is inferred
		selectionSort( arr, c);
		printArray(arr);
		
	}

}
