import java.util.Scanner;

class Data {
	private int real;
	private int imag;

	public void acceptData() {
		System.out.println("Enter real and imag values = ");
		Scanner sc = new Scanner(System.in);
		real = sc.nextInt();
		imag = sc.nextInt();
	}

	public void displayData() {
		System.out.println("Real = " + real);
		System.out.println("Imag = " + imag);
	}

}

public class Complex {

	public static void main(String[] args) {
		Data c1 = new Data();
		c1.acceptData();
		c1.displayData();
	}

}
