package minArray;

public class Main {
    public static <T> void printArray(T[] arr) {
        for(T ele : arr)
            System.out.println(ele);
        System.out.println("Number of elements printed: " + arr.length);
    }
    
    public static <T extends Number> T getMin(T[] arr) {
    	T min = arr[0];
    	for(T num:arr) {
    		if(num.doubleValue() < min.doubleValue())
    			min = num;
    	}
    	return min;
    }

	public static void main(String[] args) {
	   
	    Integer[] arr2 = { 10, 20, 30 };
	    printArray(arr2); // printArray<Integer> -- Integer type is inferred		
	    int maxInt = getMin(arr2);
	    System.out.println("Max : " + maxInt);
	}
}
