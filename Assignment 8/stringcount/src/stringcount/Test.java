package stringcount;

import java.util.function.Predicate;

public class Test {
	public static int countIf(String[] arr, Predicate<String> cond) {
		 int count = 0;
		 for(String str: arr) {
		 if(cond.test(str))
		 count++;
		 }
		 return count;
		}

	public static void main(String[] args) {
		
		String[] arr = { "abcdef", "abcde", "abcdefgh", "abcdef", "abcdef" };
		///``````
		int str=countIf(arr, new Predicate<String>() {

			@Override
			public boolean test(String t) {
			if(t.length()>=6)
				return true;
				return false;
			}
		});
		System.out.println(str);
		
		
		///```````````
		str=countIf(arr,(t)-> t.length()>=6);
		System.out.println("Total String gretter than 6 Character = "+str);
	}

}