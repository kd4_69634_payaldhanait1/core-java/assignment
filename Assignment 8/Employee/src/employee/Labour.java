package employee;

public class Labour implements Emp {
	int hours;
	int rate;

	public Labour(int hours, int rate) {
		super();
		this.hours = hours;
		this.rate = rate;
	}

	

	@Override
	public double getSal() {
		// TODO Auto-generated method stub
		return (this.hours *this.rate);
	}

	@Override
	public double calcIncentives() {
		
		if(this.hours>300)
			return this.getSal()*0.05;
		else
			return 0;
	}

}
