package Shape;

import java.util.Scanner;

public class Square extends Shape {
	private int side;

	public Square() {
		this.side = 0;
	}

	public Square(int side) {
		this.side = side;
	}

	@Override
	public void claculateArea() {
		this.area = this.side * this.side;
		System.out.println(toString());

	}

	@Override
	public String toString() {
		return "Area of Square = " + this.area;
	}

	@Override
	public void accceptRecord() {
		System.out.println("Enter side = ");
		this.side = new Scanner(System.in).nextInt();

	}

}
