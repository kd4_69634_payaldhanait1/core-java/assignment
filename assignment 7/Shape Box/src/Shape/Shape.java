package Shape;

public abstract class Shape {
	protected double area;

	public Shape() {
		this.area = 0;
	}

	public abstract void claculateArea();

	public abstract void accceptRecord();

	public double getArea() {
		return area;
	}

}
