package student;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class StudentComparator implements Comparator<Student> {

	@Override
	public int compare(Student s1, Student s2) {
		int diff = s1.getCity().compareTo(s2.getCity());
		if(diff==0)
		{
			if(s1.getMarks()>s2.getMarks())
			{
				return -1;
			}
			if(s1.getMarks()<s2.getMarks())
			{
				return +1;
			}
			else
			{
				diff =  s1.getName().compareTo(s2.getName());
				return diff;
				
			}
		}
		
		
		return -diff;
	}
}

public class Program {

	public static void main(String[] args) {
		Scanner name = new Scanner(System.in);

		Student[] stu = new Student[] { new Student(1, "Aamir", "pune", 60), new Student(2, "Cathrein", "mumbai", 40),
				new Student(3, "Carl", "pune", 66), new Student(4, "Bipasha", "sanglee", 60), new Student(5, "Balkrish", "pune", 60) };

		for (Student student : stu) {
			System.out.println(student);
		}
		System.out.println("******************************************8");
		Arrays.sort(stu, new StudentComparator());
		for (Student e : stu)
			System.out.println(e);
		System.out.println();
	}

}
