package employee;

interface Emp {
	
	public static  int salary = 0;
	public static  double incentives = 0;
	
	
	double getSal();

	default double calcIncentives() {
		return 0.0;

	}

	static double calcTotalIncome(Emp arr[]) {
	double total = 0.0;
	for (int i = 0; i < arr.length; i++) {
		total += (arr[i].getSal()+arr[i].calcIncentives()); 
	}
	
		return total;

	}
}
